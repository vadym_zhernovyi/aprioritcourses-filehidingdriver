#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    if(this->getHidingFlagRegKey())
        ui->statusLabel->setText("HIDING ON");
    else
        ui->statusLabel->setText("HIDING OFF");

    this->getRulesRegKey();

    ui->lineEdit->setPlaceholderText("Name or Path");
    ui->addRuleButton->setEnabled(false);
    ui->removeRuleButton->setEnabled(false);

    // connect for clearing lineEdit
    connect(ui->clearButton, &QPushButton::clicked, this, &MainWindow::clearLineEdit);

    // connect for enabling/disabling addRuleButton
    connect(ui->lineEdit, &QLineEdit::textChanged, this, &MainWindow::addRuleButEnable);

    // connect for enabling/disabling removeRuleButton
    connect(ui->listWidget, &QListWidget::itemClicked, this, &MainWindow::removeRuleButtonEnable);

    /*
     *   connect for send IOCTL
     */
    connect(ui->addRuleButton, &QPushButton::clicked, this, &MainWindow::pushButtonAddRule);
    connect(ui->removeRuleButton, &QPushButton::clicked, this, &MainWindow::pushButtonRemoveRule);
    connect(ui->hideButton, &QPushButton::clicked, this, &MainWindow::pushButtonHide);
    connect(ui->unhideButton, &QPushButton::clicked, this, &MainWindow::pushButtonUnhide);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::clearLineEdit()
{
    ui->lineEdit->clear();
}

void MainWindow::addRuleButEnable()
{
    QRegularExpression regex("^\\s+");

    if(ui->lineEdit->text() == "" || ui->lineEdit->text().contains(regex))
        ui->addRuleButton->setEnabled(false);
    else ui->addRuleButton->setEnabled(true);
}

void MainWindow::removeRuleButtonEnable()
{
    //ui->listWidget->clearSelection();
    ui->removeRuleButton->setEnabled(true);
}

void MainWindow::pushButtonHide()
{
   sendIOCTLflag (IOCTL_HIDE_FILE);
   ui->statusLabel->setText("HIDING ON");
   SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_IDLIST, NULL, NULL);
}

void MainWindow::pushButtonUnhide()
{
   sendIOCTLflag (IOCTL_UNHIDE_FILE);
   ui->statusLabel->setText("HIDING OFF");
   SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_IDLIST, NULL, NULL);
}

void MainWindow::pushButtonAddRule()
{
    LPCWSTR str = reinterpret_cast<LPCWSTR>(ui->lineEdit->text().unicode());
    sendIOCTLrule (IOCTL_ADD_RULE, str);
    ui->listWidget->addItem(ui->lineEdit->text());
    SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_IDLIST, NULL, NULL);
}

void MainWindow::pushButtonRemoveRule()
{
    LPCWSTR str = reinterpret_cast<LPCWSTR>(ui->listWidget->currentItem()->text().unicode());
    sendIOCTLrule (IOCTL_REMOVE_RULE, str);
    delete ui->listWidget->takeItem(ui->listWidget->row(ui->listWidget->currentItem()));
    if(!ui->listWidget->count())
        ui->removeRuleButton->setEnabled(false);
    SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_IDLIST, NULL, NULL);
}

void MainWindow::sendIOCTLrule (ULONG IoctlCode, LPCWSTR str)
{
    HANDLE hDevice;
    BOOL result;
    DWORD junk = 0;

    wchar_t Rule[256];
    wcscpy_s(Rule, str);

    wchar_t deviceName[256];
    wchar_t dosName[256];
    wchar_t resultRule[256];

    int i = 0;
    while(Rule[i])
    {
        dosName[i] = Rule[i];

        if((Rule[i] == ':' && Rule[i + 1] == '\\') || Rule[i] == ':')
        {
            dosName[i + 1] = '\0';

            if(!QueryDosDevice(dosName, deviceName, sizeof(deviceName)))
            {
                if(IoctlCode == IOCTL_ADD_RULE)
                    showError("Path is wrong!", GetLastError());
                  return;
            }
            else
            {
                  ++i;
                  int j = 0;
                  while(Rule[i])
                  {
                    resultRule[j] = Rule[i];
                    ++j;
                    ++i;
                  }
                  resultRule[j] = Rule[i];

                  QString QRule = QString::fromWCharArray(deviceName) + QString::fromWCharArray(resultRule);

                  wcscpy_s(Rule, QRule.toStdWString().c_str());
                  break;
            }
        }
        ++i;
    }

    hDevice = CreateFileA (
                "\\\\.\\HideFileFilter",
                GENERIC_READ | GENERIC_WRITE,
                0,
                NULL,
                OPEN_EXISTING,
                0,
                NULL
    );

    if (hDevice == INVALID_HANDLE_VALUE)
    {
        showError("Device is not found!", GetLastError());
        return;
    }

    result = DeviceIoControl(
                hDevice,
                IoctlCode,
                Rule,
                sizeof(Rule),
                nullptr,
                0,
                &junk,
                nullptr
    );

    if (!result)
    {
        showError("Sending data is violated", GetLastError());
    }

    CloseHandle(hDevice);

}

void MainWindow::sendIOCTLflag (ULONG IoctlCode)
{
    HANDLE hDevice;
    BOOL result;
    DWORD junk = 0;

    hDevice = CreateFileA (
                "\\\\.\\HideFileFilter",
                GENERIC_READ | GENERIC_WRITE,
                0,
                NULL,
                OPEN_EXISTING,
                0,
                NULL
    );

    if (hDevice == INVALID_HANDLE_VALUE)
    {
        showError("Device is not found!", GetLastError());
        return;
    }

    result = DeviceIoControl(
                hDevice,
                IoctlCode,
                nullptr,
                0,
                nullptr,
                0,
                &junk,
                nullptr
    );

    if (!result)
    {
        showError("Sending data is violated", GetLastError());
    }

    CloseHandle(hDevice);

}

void MainWindow::showError(const QString str, int numberError)
{
    QMessageBox mb;
    QString error = "Error #" + QString::number(numberError);
    mb.setWindowTitle(error);
    mb.setText(str);
    mb.exec();
}

bool MainWindow::getHidingFlagRegKey()
{
    QSettings settings("HKEY_LOCAL_MACHINE\\SOFTWARE\\HideDrvFlag", QSettings::NativeFormat);
    QStringList listKeys = settings.allKeys();

        if(listKeys.size())
        {
            bool flag = settings.value(listKeys[0]).toBool();
            return flag;
        }

    return false;
}

void MainWindow::getRulesRegKey()
{
    QSettings settings("HKEY_LOCAL_MACHINE\\SOFTWARE\\HideDrvRules", QSettings::NativeFormat);
    QStringList listKeys = settings.allKeys();

    if(listKeys.size())
    {
        QString stringRules = settings.value(listKeys[0]).toString();
        QStringList listRules = stringRules.split(';', QString::SkipEmptyParts);
        ui->listWidget->addItems(listRules);
    }

}

