#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <Windows.h>
#include <QSettings>
#include <QObject>
#include <QMainWindow>
#include <QMessageBox>
#include <Shlobj.h>

#include "ioctl.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    void sendIOCTLrule (ULONG IoctlCode, LPCWSTR str);
    void sendIOCTLflag (ULONG IoctlCode);
    void showError(const QString str, int numberError);
    bool getHidingFlagRegKey();
    void getRulesRegKey();

private slots:
    void clearLineEdit();
    void addRuleButEnable();
    void removeRuleButtonEnable();
    void pushButtonHide();
    void pushButtonUnhide();
    void pushButtonAddRule();
    void pushButtonRemoveRule();
};

#endif // MAINWINDOW_H
