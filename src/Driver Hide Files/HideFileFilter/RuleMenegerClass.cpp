#include "DriverClass.h"

NTSTATUS RuleManager::addRule(PUNICODE_STRING stringRule)
{
	OBJECT_ATTRIBUTES	keyObjectAttributes;
	HANDLE				keyHandle;
	NTSTATUS			keyStatus;
	ULONG				disposition, lenghtQueryValueKey = 0;
	
	UNICODE_STRING fullRule = *stringRule;
	wcscat(fullRule.Buffer, L";");


	UNICODE_STRING keyPath = RTL_CONSTANT_STRING(PATH_TO_KEY_RULES);
	UNICODE_STRING nameValue = RTL_CONSTANT_STRING(NAME_VALUE_ENTRY_RULE);

	InitializeObjectAttributes(
					&keyObjectAttributes,
					&keyPath,
					OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE,
					0, 
					0
	);

	keyStatus = ZwCreateKey(
					&keyHandle,
					KEY_SET_VALUE | KEY_WRITE,
					&keyObjectAttributes,
					0,
					NULL,
					REG_OPTION_NON_VOLATILE,
					&disposition
	);

	if (keyStatus)
		return keyStatus;

	keyStatus = ZwQueryValueKey(keyHandle, &nameValue, KeyValuePartialInformation, NULL, 0, &lenghtQueryValueKey);

	if (keyStatus == STATUS_OBJECT_NAME_NOT_FOUND)
	{
		keyStatus = ZwSetValueKey(
							keyHandle,
							&nameValue,
							0,
							REG_SZ,
							fullRule.Buffer,
							fullRule.Length
		);

		ZwFlushKey(keyHandle);
		ZwClose(keyHandle);

		return keyStatus;
	}

	PKEY_VALUE_PARTIAL_INFORMATION resultQueryValueKey =
		(PKEY_VALUE_PARTIAL_INFORMATION)ExAllocatePoolWithTag(NonPagedPool, lenghtQueryValueKey, 'Tadd');

	keyStatus = ZwQueryValueKey(
					keyHandle,
					&nameValue,
					KeyValuePartialInformation,
					resultQueryValueKey, 
					lenghtQueryValueKey,
					&lenghtQueryValueKey
	); 

	if (keyStatus)
	{
		ExFreePoolWithTag(resultQueryValueKey, 'Tadd');
		return keyStatus;
	}

	PWCHAR queriedString = (PWCHAR)ExAllocatePool(NonPagedPool, resultQueryValueKey->DataLength);
	RtlCopyMemory(queriedString, resultQueryValueKey->Data, resultQueryValueKey->DataLength);

	UNICODE_STRING allRules;
	allRules.Length = 0;
	allRules.MaximumLength = (USHORT)(fullRule.Length + resultQueryValueKey->DataLength + sizeof(UNICODE_NULL));
	allRules.Buffer = (PWSTR)ExAllocatePoolWithTag(NonPagedPool, allRules.MaximumLength, 'Tall');

	ExFreePoolWithTag(resultQueryValueKey, 'Tadd');

	if (allRules.Buffer != NULL)
	{
		NTSTATUS status = RtlAppendUnicodeToString(&allRules, queriedString);
		if (!status)
		{
			ExFreePool(queriedString);
			status = RtlAppendUnicodeStringToString(&allRules, &fullRule);
			if (status)
			{
				ExFreePoolWithTag(allRules.Buffer, 'Tall');
				
				return status;
			}
		}
		else
		{
			ExFreePoolWithTag(allRules.Buffer, 'Tall');
			ExFreePool(queriedString);
			
			return status;
		}
	}

	keyStatus = ZwSetValueKey(
					keyHandle,
					&nameValue,
					0,
					REG_SZ,
					allRules.Buffer,
					allRules.Length
	);  

	if (keyStatus)
	{
		ExFreePoolWithTag(allRules.Buffer, 'Tall');
		return keyStatus;
	}

	ZwFlushKey(keyHandle);
	ZwClose(keyHandle);

	ExFreePoolWithTag(allRules.Buffer, 'Tall');
	return keyStatus;
}

NTSTATUS RuleManager::removeRule(PUNICODE_STRING stringRule)
{
	OBJECT_ATTRIBUTES	keyObjectAttributes;
	HANDLE				keyHandle;
	NTSTATUS			keyStatus;
	ULONG				lenghtQueryValueKey = 0;

	wcscat(stringRule->Buffer, L";");

	UNICODE_STRING keyPath = RTL_CONSTANT_STRING(PATH_TO_KEY_RULES);
	UNICODE_STRING nameValue = RTL_CONSTANT_STRING(NAME_VALUE_ENTRY_RULE);

	InitializeObjectAttributes(
					&keyObjectAttributes,
					&keyPath,
					OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE,
					NULL,  
					NULL
	); 

	keyStatus = ZwOpenKey(
					&keyHandle,
					KEY_ALL_ACCESS,
					&keyObjectAttributes
	);

	if (keyStatus) return keyStatus;

	ZwQueryValueKey(keyHandle, &nameValue, KeyValuePartialInformation, NULL, 0, &lenghtQueryValueKey);
	
	PKEY_VALUE_PARTIAL_INFORMATION resultQueryValueKey =
		(PKEY_VALUE_PARTIAL_INFORMATION)ExAllocatePoolWithTag(NonPagedPool, lenghtQueryValueKey, 'teg1');

	keyStatus = ZwQueryValueKey(
					keyHandle,
					&nameValue,					
					KeyValuePartialInformation,
					resultQueryValueKey,		
					lenghtQueryValueKey,		
					&lenghtQueryValueKey
	);	

	if (keyStatus)
	{
		ExFreePoolWithTag(resultQueryValueKey, 'teg1');
		return keyStatus;
	}

	PWCHAR queriedString = (PWCHAR)ExAllocatePool(NonPagedPool, resultQueryValueKey->DataLength);
	RtlCopyMemory(queriedString, resultQueryValueKey->Data, resultQueryValueKey->DataLength);

	ExFreePoolWithTag(resultQueryValueKey, 'teg1');
	
	ULONG positionStart = 0, positionEnd = 0;
	PWCHAR buff = NULL;
	
	do
	{
		if (queriedString[positionEnd] == L';')
		{
			if (buff != NULL) ExFreePool(buff);
			ULONG numBytes = (positionEnd - positionStart + 1) * sizeof(WCHAR);
			
			buff = (PWCHAR)ExAllocatePool(NonPagedPool, numBytes + sizeof(WCHAR));
			memcpy(buff, (queriedString + positionStart), numBytes);
			buff[positionEnd - positionStart + 1] = L'\0';
			
			if (!wcscmp(buff, stringRule->Buffer))
			{
				ExFreePool(buff);
				PWCHAR newBuff1 = NULL, newBuff2 = NULL;

				if (!positionStart)									// if begin of Rules
				{
					ULONG numChars = 0,
						  position = positionEnd + 1;
					while (queriedString[position])
					{
						++position;
						++numChars;
					}
					if (!numChars)									// if one rule
					{
						keyStatus = ZwDeleteValueKey(keyHandle, &nameValue);
						ZwClose(keyHandle);

						ExFreePool(queriedString);
						return keyStatus;
					}
					
					ULONG numBytesB = (numChars + 1) * sizeof(WCHAR);

					newBuff1 = (PWCHAR)ExAllocatePool(NonPagedPool, numBytesB);
					memcpy(newBuff1, (queriedString + (positionEnd + 1)), numBytesB);
				}
				else if (!queriedString[positionEnd + 1])			// if end of Rules
				{
					ULONG numBytesE = positionStart * sizeof(WCHAR);

					newBuff1 = (PWCHAR)ExAllocatePool(NonPagedPool, numBytesE + sizeof(WCHAR));
					memcpy(newBuff1, queriedString, numBytesE);
					newBuff1[positionStart] = L'\0';
				}
				else
				{
					ULONG numChars = 0,
						position = positionEnd + 1;
					while (queriedString[position])
					{
						++position;
						++numChars;
					}
					
					ULONG numBytes2 = (numChars + 1) * sizeof(WCHAR);
					newBuff2 = (PWCHAR)ExAllocatePool(NonPagedPool, numBytes2);
					memcpy(newBuff2, (queriedString + (positionEnd + 1)), numBytes2);

					ULONG numBytes1 = positionStart * sizeof(WCHAR);
					newBuff1 = (PWCHAR)ExAllocatePool(NonPagedPool, (numBytes1 + numBytes2 + sizeof(WCHAR)));
					memcpy(newBuff1, queriedString, numBytes1);
					newBuff1[positionStart] = L'\0';

					wcscat(newBuff1, newBuff2);
					ExFreePool(newBuff2);
				}
			
				UNICODE_STRING updateRules;
				updateRules.Length = 0;
				updateRules.MaximumLength = (USHORT)(wcslen(newBuff1) * sizeof(WCHAR) + sizeof(UNICODE_NULL));
				updateRules.Buffer = (PWSTR)ExAllocatePoolWithTag(NonPagedPool, updateRules.MaximumLength, '2gat');

				RtlAppendUnicodeToString(&updateRules, newBuff1);
			
				keyStatus = ZwSetValueKey(
						keyHandle,
						&nameValue,				
						0,
						REG_SZ,
						updateRules.Buffer,
						updateRules.MaximumLength
				);	

				ZwFlushKey(keyHandle);
				ZwClose(keyHandle);

				ExFreePoolWithTag(updateRules.Buffer, '2gat');
				ExFreePool(newBuff1);
				ExFreePool(queriedString);

				return keyStatus;
			}
			else
				positionStart = (positionEnd + 1);
		}
		++positionEnd;

	} while (queriedString[positionEnd]);

	ZwClose(keyHandle);
	return keyStatus;
}

BOOLEAN RuleManager::compareRule(PUNICODE_STRING fileName, PUNICODE_STRING directoryName)
{
	OBJECT_ATTRIBUTES	keyObjectAttributes;
	HANDLE				keyHandle;
	NTSTATUS			keyStatus;
	ULONG				lenghtQueryValueKey = 0;

	UNICODE_STRING keyPath = RTL_CONSTANT_STRING(PATH_TO_KEY_RULES);
	UNICODE_STRING nameValue = RTL_CONSTANT_STRING(NAME_VALUE_ENTRY_RULE);

	InitializeObjectAttributes(
					&keyObjectAttributes,
					&keyPath,
					OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE,
					0,  
					0
	); 

	keyStatus = ZwOpenKey(
					&keyHandle,
					KEY_READ,
					&keyObjectAttributes
	);
	
	if (keyStatus == STATUS_OBJECT_NAME_NOT_FOUND)			// if key is not found
		return FALSE;
	
	keyStatus = ZwQueryValueKey(keyHandle, &nameValue, KeyValuePartialInformation, NULL, 0, &lenghtQueryValueKey);

	if (keyStatus == STATUS_OBJECT_NAME_NOT_FOUND)			// if value of key is not found
		return FALSE;
	
	PKEY_VALUE_PARTIAL_INFORMATION resultQueryValueKey =
		(PKEY_VALUE_PARTIAL_INFORMATION)ExAllocatePoolWithTag(NonPagedPool, lenghtQueryValueKey, 'Rtag');

	keyStatus = ZwQueryValueKey(
					keyHandle,
					&nameValue,
					KeyValuePartialInformation,
					resultQueryValueKey,		
					lenghtQueryValueKey,		
					&lenghtQueryValueKey
	);	

	PWCHAR queriedString = (PWCHAR)ExAllocatePool(NonPagedPool, resultQueryValueKey->DataLength);
	RtlCopyMemory(queriedString, resultQueryValueKey->Data, resultQueryValueKey->DataLength);

	ExFreePoolWithTag(resultQueryValueKey, 'Rtag');

	int positionStart = 0, positionEnd = 0;
	PWCHAR oneRule = NULL;

	do
	{
		if (queriedString[positionEnd] == L';')
		{
			ULONG numBytes = (positionEnd - positionStart) * sizeof(WCHAR);

			oneRule = (PWCHAR)ExAllocatePool(NonPagedPool, numBytes + sizeof(WCHAR));
			memcpy(oneRule, (queriedString + positionStart), numBytes);
			oneRule[positionEnd - positionStart] = L'\0';

			UNICODE_STRING oneRuleUS;
			oneRuleUS.Length = 0;
			oneRuleUS.MaximumLength = (USHORT)(numBytes + sizeof(WCHAR));
			oneRuleUS.Buffer = (PWSTR)ExAllocatePoolWithTag(NonPagedPool, oneRuleUS.MaximumLength, 'Tone');

			RtlAppendUnicodeToString(&oneRuleUS, oneRule);

			UNICODE_STRING _directoryName;
			_directoryName.Length = 0;
			_directoryName.MaximumLength = directoryName->MaximumLength;
			_directoryName.Buffer = (PWSTR)ExAllocatePoolWithTag(NonPagedPool, _directoryName.MaximumLength, 'ypoC');

			RtlAppendUnicodeToString(&_directoryName, directoryName->Buffer);
			
			UNICODE_STRING path, name;
			path.Buffer = NULL;

			if (!separatePathName(&path, &name, &oneRuleUS))
			{
				if (FsRtlIsNameInExpression(&oneRuleUS, fileName, FALSE, 0))
				{
					ZwFlushKey(keyHandle);
					ZwClose(keyHandle);

					ExFreePoolWithTag(_directoryName.Buffer, 'ypoC');
					ExFreePool(oneRule);
					ExFreePoolWithTag(oneRuleUS.Buffer, 'Tone');
					ExFreePool(queriedString);

					return TRUE;
				}
			}
			else
			{
				if (_directoryName.Buffer[_directoryName.Length / sizeof(WCHAR) - 1] == '\\')
				{
					_directoryName.Length -= sizeof(WCHAR);
					_directoryName.MaximumLength -= sizeof(WCHAR);
					_directoryName.Buffer[_directoryName.Length / sizeof(WCHAR)] = '\0';
				}

				if (FsRtlIsNameInExpression(&path, &_directoryName, FALSE, 0))
				{
					if (FsRtlIsNameInExpression(&name, fileName, FALSE, 0))
					{
						ZwFlushKey(keyHandle);
						ZwClose(keyHandle);

						ExFreePoolWithTag(_directoryName.Buffer, 'ypoC');
						ExFreePool(path.Buffer);
						ExFreePool(oneRule);
						ExFreePoolWithTag(oneRuleUS.Buffer, 'Tone');
						ExFreePool(queriedString);

						return TRUE;
					}
				}
			}
			
			positionStart = (positionEnd + 1);

			if (path.Buffer != NULL)
				ExFreePool(path.Buffer);

			ExFreePoolWithTag(_directoryName.Buffer, 'ypoC');
			ExFreePoolWithTag(oneRuleUS.Buffer, 'Tone');
			ExFreePool(oneRule);
		}
		++positionEnd;

	} while (queriedString[positionEnd]);

	ZwClose(keyHandle);
	ExFreePool(queriedString);

	return FALSE;
}

BOOLEAN RuleManager::separatePathName(PUNICODE_STRING path, PUNICODE_STRING name, PUNICODE_STRING pathName)
{
	ULONG i = pathName->Length / sizeof(WCHAR);

	if (pathName->Buffer[i - 1] == L'\\')				// name isn't pointed
		return FALSE;

	for (;;)
	{
		--i;
		
		if (i == 0)										// the whole thing is a file name 
			return FALSE;

		if (pathName->Buffer[i] == L'\\')
		{
			// separating name
			*name = *pathName;
			name->Buffer += i + 1;
			name->Length -= (USHORT)((i + 1) * sizeof(WCHAR));
			
			// separating path
			USHORT numBytes = (USHORT)((i + 1) * sizeof(WCHAR));
			PWCHAR _path = (PWCHAR)ExAllocatePool(NonPagedPool, numBytes);
			memcpy(_path, pathName->Buffer, numBytes - sizeof(WCHAR));
			_path[i] = L'\0';

			path->Length = 0;
			path->MaximumLength = numBytes;
			path->Buffer = (PWSTR)ExAllocatePool(NonPagedPool, path->MaximumLength);

			RtlAppendUnicodeToString(path, _path);

			ExFreePool(_path);
			return TRUE;
		}
	}
}