#ifndef _DRIVERCLASS_H_
#define _DRIVERCLASS_H_

#include <fltKernel.h>
#include <dontuse.h>
#include <suppress.h>

#include "MinifilterClass.h"
#include "RuleManagerClass.h"
#include "IOCTL.h"

class Driver
{
	Minifilter minifilter;
	RuleManager ruleManager;
	PDEVICE_OBJECT DeviceObject;
	UNICODE_STRING DeviceName;
	DWORD32 hidingFlag;
	
public:

	static Driver& create();
	static VOID destroy();
	static Driver& getInstance();
	RuleManager& getRuleManager();

	NTSTATUS FLTCreate(_In_ PDRIVER_OBJECT DriverObject);
	NTSTATUS FLTStart();
	VOID FLTDestroy();

	NTSTATUS CreateDevice(_In_ PDRIVER_OBJECT DriverObject);
	VOID DeleteDevice();

	NTSTATUS setHidingFlagReg(DWORD32 flag);
	NTSTATUS getHidingFlagReg(PDWORD32 flag);
	VOID setHidingFlag(DWORD32 flag);
	DWORD32 getHidingFlag();

	static NTSTATUS DispatchCreateClose(PDEVICE_OBJECT DeviceObject, PIRP Irp);
	static NTSTATUS DispatchDeviceIoCtl(PDEVICE_OBJECT DeviceObject, PIRP Irp);
};

VOID DriverUnload(_In_ PDRIVER_OBJECT DriverObject);

CONST FLT_REGISTRATION FilterRegistration =
{
	sizeof(FLT_REGISTRATION),
	FLT_REGISTRATION_VERSION,
	0,
	NULL,
	Callbacks,
	Minifilter::FilterUnload,
	Minifilter::FilterLoad,
	Minifilter::QueryTeardown,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL
};

#endif
