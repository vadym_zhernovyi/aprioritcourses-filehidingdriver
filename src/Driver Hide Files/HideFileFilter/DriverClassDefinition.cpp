#include "DriverClass.h"

static Driver* g_driver = nullptr;

Driver& Driver::create()
{
	g_driver = reinterpret_cast<Driver*>(ExAllocatePoolWithTag(NonPagedPoolNx, sizeof(Driver), 'hdrv'));
	return *g_driver;
}

VOID Driver::destroy()
{
	ExFreePoolWithTag(g_driver, 'hdrv');
}

Driver& Driver::getInstance()
{
	return *reinterpret_cast<Driver*>(g_driver);
}

RuleManager& Driver::getRuleManager()
{
	return ruleManager;
}

NTSTATUS Driver::FLTCreate (_In_ PDRIVER_OBJECT DriverObject)
{
	return minifilter.FilterRegister(DriverObject);
}

NTSTATUS Driver::FLTStart()
{
	return minifilter.FilterStart();
}

VOID Driver::FLTDestroy()
{
	minifilter.FilterUnregister();
}

NTSTATUS Driver::CreateDevice(_In_ PDRIVER_OBJECT DriverObject)
{
	UNICODE_STRING symbolicLinkName;
	NTSTATUS status;

	DeviceName = RTL_CONSTANT_STRING(HIDEDRV_DEVICE_NAME);
	symbolicLinkName = RTL_CONSTANT_STRING(SYMBOLIC_NAME_STRING);

	status = IoCreateDevice(
				DriverObject,
				0,
				&DeviceName,
				FILE_DEVICE_UNKNOWN,
				FILE_DEVICE_SECURE_OPEN,
				FALSE,
				&DeviceObject);

	if (NT_SUCCESS(status)) 
		status = IoCreateSymbolicLink(&symbolicLinkName, &DeviceName);

	return status;
}

VOID Driver::DeleteDevice()
{
	IoDeleteDevice(DeviceObject);
}

NTSTATUS Driver::setHidingFlagReg(DWORD32 flag)
{
	OBJECT_ATTRIBUTES oa;
	UNICODE_STRING keyPath;

	RtlInitUnicodeString(&keyPath, PATH_TO_KEY_FLAG);

	InitializeObjectAttributes(&oa, &keyPath, OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, 0, 0);
	
	ULONG disposition;
	HANDLE hKey;
	NTSTATUS status;
	status = ZwCreateKey(&hKey, KEY_SET_VALUE | KEY_WRITE, &oa, 0, NULL, REG_OPTION_NON_VOLATILE, &disposition);
	
	if (!NT_SUCCESS(status)) return status;

	UNICODE_STRING nameValue = RTL_CONSTANT_STRING(NAME_VALUE_ENTRY_FLAG);
	status = ZwSetValueKey(hKey, &nameValue, 0, REG_DWORD, &flag, sizeof(flag));
	
	if (!NT_SUCCESS(status)) return status;

	status = ZwFlushKey(hKey);
	
	if (!NT_SUCCESS(status)) return status;

	ZwClose(hKey);

	return status;
}

NTSTATUS Driver::getHidingFlagReg(PDWORD32 flag)
{
	HANDLE hKey = NULL;
	OBJECT_ATTRIBUTES oa;
	UNICODE_STRING keyPath, nameValue;
	ULONG returnLength = 0;

	RtlInitUnicodeString(&keyPath, PATH_TO_KEY_FLAG);
	RtlInitUnicodeString(&nameValue, NAME_VALUE_ENTRY_FLAG);

	InitializeObjectAttributes(&oa, &keyPath, OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE, 0, 0);

	NTSTATUS status = ZwOpenKey(&hKey, KEY_READ, &oa);
	if (!NT_SUCCESS(status)) return status;

	ZwQueryValueKey(hKey, &nameValue, KeyValuePartialInformation, NULL, 0, &returnLength);
	
	PKEY_VALUE_PARTIAL_INFORMATION buffer = 
		(PKEY_VALUE_PARTIAL_INFORMATION)ExAllocatePoolWithTag(NonPagedPool, returnLength, 'Ateg');

	status = ZwQueryValueKey(hKey, &nameValue, KeyValuePartialInformation, buffer, returnLength, &returnLength);
	
	if (!NT_SUCCESS(status))
	{
		ExFreePoolWithTag(buffer, 'Ateg');
		return status;
	}
		
	*flag = buffer->Data[0];

	ZwClose(hKey);
	ExFreePoolWithTag(buffer, 'Ateg');

	return status;
}

VOID Driver::setHidingFlag(DWORD32 flag)
{
	hidingFlag = flag;
}

DWORD32 Driver::getHidingFlag()
{
	return hidingFlag;
}

NTSTATUS Driver::DispatchCreateClose(
	_In_ PDEVICE_OBJECT DeviceObject,
	_In_ PIRP Irp
)
{
	UNREFERENCED_PARAMETER(DeviceObject);

	Irp->IoStatus.Status = STATUS_SUCCESS;
	IoCompleteRequest(Irp, IO_NO_INCREMENT);

	return STATUS_SUCCESS;
}

NTSTATUS Driver::DispatchDeviceIoCtl(
	_In_ PDEVICE_OBJECT DeviceObject,
	_In_ PIRP Irp
)
{
	UNREFERENCED_PARAMETER(DeviceObject);
	auto& ruleManager = getInstance().getRuleManager();
	PIO_STACK_LOCATION IrpSL = IoGetCurrentIrpStackLocation(Irp);
	UNICODE_STRING rule;
	NTSTATUS status;

	rule.Buffer = (PWCHAR)Irp->AssociatedIrp.SystemBuffer;
	rule.Length = (USHORT)IrpSL->Parameters.DeviceIoControl.InputBufferLength;
	rule.MaximumLength = rule.Length;
	
	switch (IrpSL->Parameters.DeviceIoControl.IoControlCode)
	{
	case IOCTL_HIDE_FILE:

		getInstance().setHidingFlag(1);
		status = getInstance().setHidingFlagReg(1);

		Irp->IoStatus.Status = status;
		IoCompleteRequest(Irp, IO_NO_INCREMENT);
		return status;

	case IOCTL_UNHIDE_FILE:

		getInstance().setHidingFlag(0);
		status = getInstance().setHidingFlagReg(0);
		
		Irp->IoStatus.Status = status;
		IoCompleteRequest(Irp, IO_NO_INCREMENT);
		return status;

	case IOCTL_ADD_RULE:

		status = ruleManager.addRule(&rule);
		
		Irp->IoStatus.Status = status;
		IoCompleteRequest(Irp, IO_NO_INCREMENT);
		return status;

	case IOCTL_REMOVE_RULE:

		status = ruleManager.removeRule(&rule);
		
		Irp->IoStatus.Status = status;
		IoCompleteRequest(Irp, IO_NO_INCREMENT);
		return status;

	default:
		Irp->IoStatus.Status = STATUS_NOT_SUPPORTED;
		IoCompleteRequest(Irp, IO_NO_INCREMENT);

		return STATUS_NOT_SUPPORTED;
	}
}