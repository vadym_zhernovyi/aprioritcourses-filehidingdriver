#ifndef _RULEMANAGERCLASS_H_
#define _RULEMANAGERCLASS_H_

class RuleManager
{
public:
	
	NTSTATUS addRule(PUNICODE_STRING stringRule);

	NTSTATUS removeRule(PUNICODE_STRING stringRule);

	BOOLEAN compareRule(PUNICODE_STRING fileName, PUNICODE_STRING directoryName);

	BOOLEAN separatePathName(PUNICODE_STRING path, PUNICODE_STRING name, PUNICODE_STRING pathName);
};

#endif
