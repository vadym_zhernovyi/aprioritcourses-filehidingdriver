#include "DriverClass.h"

EXTERN_C DRIVER_INITIALIZE DriverEntry;

NTSTATUS DriverEntry(
	_In_ PDRIVER_OBJECT DriverObject,
	_In_ PUNICODE_STRING RegistryPath
)
{
	UNREFERENCED_PARAMETER(RegistryPath);

	NTSTATUS status = Driver::create().FLTCreate(DriverObject);
	
	if (NT_SUCCESS(status))
	{
		status = Driver::getInstance().FLTStart();
		
		if (!NT_SUCCESS(status))
		{
			Driver::getInstance().FLTDestroy();
			return status;
		}
	}
	else
	{
		return status;
	}

	status = Driver::getInstance().CreateDevice(DriverObject);
	
	if (!NT_SUCCESS(status))
	{
		Driver::getInstance().FLTDestroy();
		return status;
	}

	DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = Driver::DispatchDeviceIoCtl;
	DriverObject->MajorFunction[IRP_MJ_CREATE] = Driver::DispatchCreateClose;
	DriverObject->MajorFunction[IRP_MJ_CLOSE] = Driver::DispatchCreateClose;
	DriverObject->DriverUnload = DriverUnload;
	
	return status;
}

VOID DriverUnload(_In_ PDRIVER_OBJECT DriverObject)
{
	UNREFERENCED_PARAMETER(DriverObject);
	Driver::getInstance().DeleteDevice();
	Driver::getInstance().FLTDestroy();
	Driver::destroy();
}