#include "DriverClass.h"

NTSTATUS FLTAPI Minifilter::FilterUnload(_In_ FLT_FILTER_UNLOAD_FLAGS Flags)
{
	UNREFERENCED_PARAMETER(Flags);
	return STATUS_SUCCESS;
}

NTSTATUS FLTAPI Minifilter::FilterLoad(
	IN PCFLT_RELATED_OBJECTS  FltObjects,
	IN FLT_INSTANCE_SETUP_FLAGS  Flags,
	IN DEVICE_TYPE  VolumeDeviceType,
	IN FLT_FILESYSTEM_TYPE  VolumeFilesystemType
)
{
	UNREFERENCED_PARAMETER(FltObjects);
	UNREFERENCED_PARAMETER(Flags);
	UNREFERENCED_PARAMETER(VolumeDeviceType);
	UNREFERENCED_PARAMETER(VolumeFilesystemType);
	return STATUS_SUCCESS;
}

NTSTATUS Minifilter::FilterRegister(_In_ PDRIVER_OBJECT DriverObject)
{
	return FltRegisterFilter(DriverObject, &FilterRegistration, &FilterHandle);
}

NTSTATUS Minifilter::FilterStart()
{
	return FltStartFiltering(FilterHandle);
}

VOID Minifilter::FilterUnregister()
{
	FltUnregisterFilter(FilterHandle);
}

NTSTATUS Minifilter::QueryTeardown(
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_In_ FLT_INSTANCE_QUERY_TEARDOWN_FLAGS Flags
)
{
	UNREFERENCED_PARAMETER(FltObjects);
	UNREFERENCED_PARAMETER(Flags);

	return STATUS_SUCCESS;
}

FLT_PREOP_CALLBACK_STATUS Minifilter::PreDirectoryControl(
	_Inout_ PFLT_CALLBACK_DATA Data,
	_In_ PCFLT_RELATED_OBJECTS FltObjects,
	_Flt_CompletionContext_Outptr_ PVOID *CompletionContext
)
{
	UNREFERENCED_PARAMETER(FltObjects);
	UNREFERENCED_PARAMETER(CompletionContext);
	
	if (Data->Iopb->MinorFunction != IRP_MN_QUERY_DIRECTORY)
		return FLT_PREOP_SUCCESS_NO_CALLBACK;

	switch (Data->Iopb->Parameters.DirectoryControl.QueryDirectory.FileInformationClass)
	{
	case FileIdFullDirectoryInformation:
	case FileIdBothDirectoryInformation:
	case FileBothDirectoryInformation:
	case FileDirectoryInformation:
	case FileFullDirectoryInformation:
	case FileNamesInformation:
		break;

	default:
		return FLT_PREOP_SUCCESS_NO_CALLBACK;
	}

	return FLT_PREOP_SUCCESS_WITH_CALLBACK;
}

FLT_POSTOP_CALLBACK_STATUS Minifilter::PostDirectoryControl(
	_Inout_   PFLT_CALLBACK_DATA Data,
	_In_      PCFLT_RELATED_OBJECTS FltObjects,
	_In_opt_  PVOID CompletionContext,
	_In_      FLT_POST_OPERATION_FLAGS Flags
)
{
	UNREFERENCED_PARAMETER(CompletionContext);
	NTSTATUS status = Data->IoStatus.Status;
	
	if (!NT_SUCCESS(status) || Flags & FLTFL_POST_OPERATION_DRAINING || 
		STATUS_REPARSE == status || FltObjects->FileObject->Flags & (FO_NAMED_PIPE | FO_MAILSLOT | FO_VOLUME_OPEN) || 
		::IoGetTopLevelIrp() || ::FsRtlIsPagingFile(FltObjects->FileObject))
	{
		return FLT_POSTOP_FINISHED_PROCESSING;
	}

	switch (Data->Iopb->Parameters.DirectoryControl.QueryDirectory.FileInformationClass)
	{
	case FileDirectoryInformation:
		return Hiding<PFILE_DIRECTORY_INFORMATION, FILE_DIRECTORY_INFORMATION>(Data);

	case FileFullDirectoryInformation:
		return Hiding<PFILE_FULL_DIR_INFORMATION, FILE_FULL_DIR_INFORMATION>(Data);

	case FileNamesInformation:
		return Hiding<PFILE_NAMES_INFORMATION, FILE_NAMES_INFORMATION>(Data);

	case FileBothDirectoryInformation:
		return Hiding<PFILE_BOTH_DIR_INFORMATION, FILE_BOTH_DIR_INFORMATION>(Data);

	case FileIdBothDirectoryInformation:
		return Hiding<PFILE_ID_BOTH_DIR_INFORMATION, FILE_ID_BOTH_DIR_INFORMATION>(Data);

	case FileIdFullDirectoryInformation:
		return Hiding<PFILE_ID_FULL_DIR_INFORMATION, FILE_ID_FULL_DIR_INFORMATION>(Data);

	default:
		return FLT_POSTOP_FINISHED_PROCESSING;
	}
}

template <typename PFILE_INFO, typename FILE_INFO>
FLT_POSTOP_CALLBACK_STATUS Minifilter::Hiding(PFLT_CALLBACK_DATA Data)
{
	auto& ruleManager = Driver::getInstance().getRuleManager();
	PFILE_INFO fileInfo, lastfileInfo, nextfileInfo;
	PFLT_FILE_NAME_INFORMATION info = nullptr; 
	UNICODE_STRING fileName;
	ULONG moveLength;
	BOOLEAN flag = false;
	static USHORT breakAccessToReg = 0;
	
	if (!breakAccessToReg)
	{
		DWORD32 flagReg;
		NTSTATUS keyStatus = Driver::getInstance().getHidingFlagReg(&flagReg);
		if (keyStatus)
			Driver::getInstance().setHidingFlag(0);
		else
			Driver::getInstance().setHidingFlag(flagReg);
		++breakAccessToReg;
	}

	DWORD32 hidingFlag = Driver::getInstance().getHidingFlag();
	
	if(!hidingFlag)
		return FLT_POSTOP_FINISHED_PROCESSING;

	FltGetFileNameInformation(Data, FLT_FILE_NAME_NORMALIZED | FLT_FILE_NAME_QUERY_DEFAULT, &info);

	lastfileInfo = NULL;
	fileInfo = (PFILE_INFO)Data->Iopb->Parameters.DirectoryControl.QueryDirectory.DirectoryBuffer;

	for (;;)
	{
		fileName.Buffer = fileInfo->FileName;
		fileName.Length = (USHORT)fileInfo->FileNameLength;
		fileName.MaximumLength = fileName.Length;
	
		if (ruleManager.compareRule(&fileName, &info->Name))
		{
			flag = true;
			if (lastfileInfo != NULL)
				fileInfo->NextEntryOffset != 0 ? lastfileInfo->NextEntryOffset += fileInfo->NextEntryOffset : lastfileInfo->NextEntryOffset = 0;
			else
			{
				if (fileInfo->NextEntryOffset != 0)
				{
					nextfileInfo = (PFILE_INFO)((PUCHAR)fileInfo + fileInfo->NextEntryOffset);
					moveLength = 0;
					while (nextfileInfo->NextEntryOffset != 0)
					{
						moveLength += FIELD_OFFSET(FILE_INFO, FileName) + nextfileInfo->FileNameLength;
						nextfileInfo = (PFILE_INFO)((PUCHAR)nextfileInfo + nextfileInfo->NextEntryOffset);
					}
					moveLength += FIELD_OFFSET(FILE_INFO, FileName) + nextfileInfo->FileNameLength;

					RtlMoveMemory(
						fileInfo,
						(PUCHAR)fileInfo + fileInfo->NextEntryOffset,
						moveLength);
				}
				else
				{
					Data->IoStatus.Status = STATUS_NO_MORE_ENTRIES;
					return FLT_POSTOP_FINISHED_PROCESSING;
				}
			}
		}

		if (!flag)
			lastfileInfo = fileInfo;
		else if (!lastfileInfo)
			continue;
		else if (lastfileInfo->NextEntryOffset == 0)
			break;

		fileInfo = (PFILE_INFO)((PUCHAR)fileInfo + fileInfo->NextEntryOffset);
		flag = false;

		if (lastfileInfo == fileInfo)
			break;
	}

	FltReleaseFileNameInformation(info);
	return FLT_POSTOP_FINISHED_PROCESSING;
}