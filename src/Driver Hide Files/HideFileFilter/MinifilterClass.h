#ifndef _MINIFILTERCLASS_H_
#define _MINIFILTERCLASS_H_

class Minifilter
{
	PFLT_FILTER FilterHandle;

public:

	NTSTATUS FilterRegister(_In_ PDRIVER_OBJECT DriverObject);

	NTSTATUS FilterStart();

	VOID FilterUnregister();

	static NTSTATUS FLTAPI QueryTeardown(
		_In_ PCFLT_RELATED_OBJECTS FltObjects,
		_In_ FLT_INSTANCE_QUERY_TEARDOWN_FLAGS Flags
	);

	static FLT_PREOP_CALLBACK_STATUS FLTAPI PreDirectoryControl(
		_Inout_ PFLT_CALLBACK_DATA Data,
		_In_ PCFLT_RELATED_OBJECTS FltObjects,
		_Flt_CompletionContext_Outptr_ PVOID *CompletionContext
	);

	static FLT_POSTOP_CALLBACK_STATUS FLTAPI PostDirectoryControl(
		_Inout_   PFLT_CALLBACK_DATA Data,
		_In_      PCFLT_RELATED_OBJECTS FltObjects,
		_In_opt_  PVOID CompletionContext,
		_In_      FLT_POST_OPERATION_FLAGS Flags
	);

	template <typename PFILE_INFO, typename FILE_INFO>
	static FLT_POSTOP_CALLBACK_STATUS Hiding(PFLT_CALLBACK_DATA Data);

	static NTSTATUS FLTAPI FilterUnload(FLT_FILTER_UNLOAD_FLAGS Flags);
	static NTSTATUS FLTAPI FilterLoad(
		IN PCFLT_RELATED_OBJECTS  FltObjects,
		IN FLT_INSTANCE_SETUP_FLAGS  Flags,
		IN DEVICE_TYPE  VolumeDeviceType,
		IN FLT_FILESYSTEM_TYPE  VolumeFilesystemType
	);
};

CONST FLT_OPERATION_REGISTRATION Callbacks[] =
{
	{
		IRP_MJ_DIRECTORY_CONTROL,
		0,
		Minifilter::PreDirectoryControl,
		Minifilter::PostDirectoryControl
	},

	{ IRP_MJ_OPERATION_END }
};

#endif