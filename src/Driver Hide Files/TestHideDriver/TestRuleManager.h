#pragma once

#include <FltKernel.h>
#include "kmtest\include\kmtest\kmtest.h"

class TestRuleManager
{
public:

	static NTSTATUS testAddRule(PUNICODE_STRING stringRule);

	static NTSTATUS testRemoveRule(PUNICODE_STRING stringRule);

	static BOOLEAN testCompareRule(PUNICODE_STRING fileName, PUNICODE_STRING directoryName);

	static BOOLEAN testSeparatePathName(PUNICODE_STRING path, PUNICODE_STRING name, PUNICODE_STRING pathName);
};


