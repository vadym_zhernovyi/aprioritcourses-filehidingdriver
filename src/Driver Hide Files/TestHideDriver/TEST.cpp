#include "TestRuleManager.h"

WCHAR _rule1Add[256] = L"FolderName";
WCHAR _rule2Add[256] = L"\\Device\\HarddiskVolume1\\SomeFolder\\SomeFoler\\someFile.asd";
WCHAR _rule3Add[256] = L"somefileName.opi";

WCHAR _rule1Rem[256] = L"FolderName";
WCHAR _rule2Rem[256] = L"\\Device\\HarddiskVolume1\\SomeFolder\\SomeFoler\\someFile.asd";
WCHAR _rule3Rem[256] = L"somefileName.opi";

NTSTATUS TESTstatus = STATUS_SUCCESS;

SCENARIO("Adding rules")
{
	GIVEN("Empty key")
	{
		WHEN("FolderName")
		{
			THEN("status will be STATUS_SUCCESS")
			{
				UNICODE_STRING rule = RTL_CONSTANT_STRING(_rule1Add);
				REQUIRE(TestRuleManager::testAddRule(&rule) == TESTstatus);
			}
		}
	}
	GIVEN("FolderName;")
	{
		WHEN("\\Device\\HarddiskVolume1\\SomeFolder\\SomeFoler\\someFile.asd")
		{
			THEN("status will be STATUS_SUCCESS")
			{
				UNICODE_STRING rule = RTL_CONSTANT_STRING(_rule2Add);
				REQUIRE(TestRuleManager::testAddRule(&rule) == TESTstatus);
			}
		}
	}
	GIVEN("FolderName;\\Device\\HarddiskVolume1\\SomeFolder\\SomeFoler\\someFile.asd")
	{
		WHEN("somefileName.opi")
		{
			THEN("status will be STATUS_SUCCESS")
			{
				UNICODE_STRING rule = RTL_CONSTANT_STRING(_rule3Add);
				REQUIRE(TestRuleManager::testAddRule(&rule) == TESTstatus);
			}
		}
	}
	GIVEN("FolderName;\\Device\\HarddiskVolume1\\SomeFolder\\SomeFoler\\someFile.asd;somefileName.opi;")
	{
		WHEN("Empty string")
		{
			THEN("status will not be STATUS_SUCCESS")
			{
				UNICODE_STRING rule = RTL_CONSTANT_STRING(L"");
				REQUIRE(TestRuleManager::testAddRule(&rule) != TESTstatus);
			}
		}
	}
}

SCENARIO("Camparing rules")
{
	GIVEN("FolderName;\\Device\\HarddiskVolume1\\SomeFolder\\SomeFoler\\someFile.asd;somefileName.opi;")
	{
		WHEN("FolderName")
		{
			THEN("will be TRUE")
			{
				UNICODE_STRING name = RTL_CONSTANT_STRING(L"FolderName");
				UNICODE_STRING somePath = RTL_CONSTANT_STRING(L"");
				
				REQUIRE(TestRuleManager::testCompareRule(&name, &somePath) == TRUE);
			}
		}
		WHEN("\\Device\\HarddiskVolume1\\SomeFolder\\SomeFoler\\someFile.asd")
		{
			THEN("will be TRUE")
			{
				UNICODE_STRING name = RTL_CONSTANT_STRING(L"someFile.asd");
				UNICODE_STRING path = RTL_CONSTANT_STRING(L"\\Device\\HarddiskVolume1\\SomeFolder\\SomeFoler");
				
				REQUIRE(TestRuleManager::testCompareRule(&name, &path) == TRUE);
			}
		}
		WHEN("\\Device\\HarddiskVolume1\\SomeFolder\\nameFolder")
		{
			THEN("will be FALSE")
			{
				UNICODE_STRING name = RTL_CONSTANT_STRING(L"nameFolder");
				UNICODE_STRING path = RTL_CONSTANT_STRING(L"\\Device\\HarddiskVolume1\\SomeFolder");
				
				REQUIRE(TestRuleManager::testCompareRule(&name, &path) == FALSE);
			}
		}
		WHEN("fileName.txt")
		{
			THEN("will be FALSE")
			{
				UNICODE_STRING name = RTL_CONSTANT_STRING(L"fileName.txt");
				UNICODE_STRING somePath = RTL_CONSTANT_STRING(L"");

				REQUIRE(TestRuleManager::testCompareRule(&name, &somePath) == FALSE);
			}
		}
	}
}

SCENARIO("Removing rules")
{
	GIVEN("FolderName;\\Device\\HarddiskVolume1\\SomeFolder\\SomeFoler\\someFile.asd;somefileName.opi;")
	{
		WHEN("\\Device\\HarddiskVolume1\\SomeFolder\\SomeFoler\\someFile.asd")
		{
			THEN("status will be STATUS_SUCCESS")
			{
				UNICODE_STRING rule = RTL_CONSTANT_STRING(_rule2Rem);
				REQUIRE(TestRuleManager::testRemoveRule(&rule) == TESTstatus);
			}
		}
	}
	GIVEN("FolderName;somefileName.opi;")
	{
		WHEN("somefileName.opi")
		{
			THEN("status will be STATUS_SUCCESS")
			{
				UNICODE_STRING rule = RTL_CONSTANT_STRING(_rule3Rem);
				REQUIRE(TestRuleManager::testRemoveRule(&rule) == TESTstatus);
			}
		}
	}
	GIVEN("FolderName;")
	{
		WHEN("FolderName")
		{
			THEN("status will be STATUS_SUCCESS")
			{
				UNICODE_STRING rule = RTL_CONSTANT_STRING(_rule1Rem);
				REQUIRE(TestRuleManager::testRemoveRule(&rule) == TESTstatus);
			}
		}
	}
}